
import Map from "../Mapbox/Map";
import Form from "../modules/FormLocalEvent"
import Header from "../modules/Header";


export default class Home {

    map
    form
    headerContainer
    header
    sidebar
    constructor() {
        // injection du code html de la page d'accueil
        document.body.innerHTML = this.content();
        // récupération des conteneurs
        this.sidebar = document.querySelector('#sidebar');
        this.headerContainer = document.querySelector('#header');

        // création de la map (injectée automatiquement dans le conteneur main-map)
        this.map = new Map();
        this.render();


    }

    // HTML de la page d'accueil
    content() {

        return `
        <header id="header"></header>
        <div id="main-map"></div>

        <div id="sidebar">
        </div>
        `
    }
    render() {
        this.clear();
        // injection du formulaire si un utilisateur est detecté
        if (User) {
            this.form = new Form(this.sidebar, this.map);
        }
        // injection du header
        this.header = new Header(this);
    }
    clear() {
        // reset des conteneurs
        this.sidebar.innerHTML = '';
        this.headerContainer.innerHTML = '';

    }

}