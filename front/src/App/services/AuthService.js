export default class AuthService {

    login(User, AuthModale) {
        fetch('http://localhost:3000/api/auth/login', {
            method: "POST",
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(User),
        })
            .then(response => response.json())
            .then(json => {
                // le stockage du token uniquement en localstorage comporte une faille
                // https://www.codeheroes.fr/2020/06/20/securiser-une-api-rest-3-3-gestion-du-jwt-cote-client/
                if (json.error) return console.log(json.error);;
                window.User = JSON.stringify(json.token);
                localStorage.setItem('token', JSON.stringify(json.token));
                AuthModale.removeModale();
                AuthModale.container.page.render();
            }
            )
            .catch(err => console.log(err));

    };
}