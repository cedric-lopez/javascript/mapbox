export default class EventService {

  postLocalEvent(LocalEvent, form) {
    const token = JSON.parse(User);
    fetch('http://localhost:3000/api/LocalEvent', {
      method: "POST",
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(LocalEvent),
    })
      .then(response => response.json())
      .then(json => {
        if (json.error) {
          form.containerMessage.innerHTML = "Evènement non enregistré"
          form.addErrors(json.error.errors);
          return
        }
        form.containerMessage.innerHTML = "Evènement enregistré"
        form.clear();
        form.createLocalEvent(json);
      }
        // TODO mettre le message de confirmation dans une popup
      )
      .catch(err => console.log(err));

  };
  getLocalEvents(map) {
    fetch('http://localhost:3000/api/LocalEvent', {
      method: "GET",
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json',
      },

    })
      .then(response => response.json())
      .then(data => {
        map.addEvents(data)
      })
      .catch(err => console.log(err));
  };
  deleteLocalEvent(eventId, marker) {
    const token = JSON.parse(User);
    fetch(`http://localhost:3000/api/LocalEvent/${eventId}`, {
      method: 'DELETE',
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
      .then(res => res.json())
      .then(res => {
        if (res.error) return
        console.log(res)
      }
      )
  }
}