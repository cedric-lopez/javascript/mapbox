// TODO Faire une seule classe modale et des classes étendues
// TODO Gestion des messages d'erreurs
import AuthService from "../services/AuthService";
export default class AuthModale {
    overlay;
    modale;
    service;
    inputs = {};
    container;
    constructor(container) {
        this.overlay = document.createElement('div');
        this.modale = document.createElement('div');
        this.overlay.classList.add("overlay");
        this.modale.classList.add("modale");
        document.body.append(this.overlay)
        document.body.append(this.modale)
        this.overlay.addEventListener('click', this.removeModale.bind(this));
        this.addModaleContent();
        this.inputsContainers()
        this.service = new AuthService()
        this.container = container
        this.container.submit.remove();

    }

    addModaleContent() {
        const containerButton = document.createElement('div');
        containerButton.classList.add("center");
        containerButton.classList.add("marg-top");
        const button = document.createElement('button');
        button.setAttribute('type', 'submit');
        containerButton.append(button);
        button.innerHTML = `
        Valider
        `
        this.modale.innerHTML = `
        <h2>Connexion</h2>
        <form>
        <div class='flex-column marg-top'>
            <label for="email">Email:</label>  
            <p>admin@admin.com</p>
            <input type="text" id="email" name="email" required>
        </div>
        <div class='flex-column marg-top'>
            <label for="password">Mot de passe:</label>
            <p>admin</p>
            <input type="password" id="password" name="password" required autocomplete="on">
        </div>
        </form>
        `
        this.modale.append(containerButton);
        button.addEventListener('click', this.conexion.bind(this))
    }
    // attribution des champs de formulaire dans l'objet form
    inputsContainers() {

        const containersInputs = this.modale.querySelectorAll('input,textarea');
        for (let value of containersInputs) {
            this.inputs[`${value.id}`] = value;
        }

    }
    removeModale() {
        this.container.addsubmit()
        this.overlay.remove();
        this.modale.remove();
    }
    conexion() {
        const NewUser = {
        }
        for (let input in this.inputs) {
            NewUser[`${input}`] = this.inputs[input].value;

        }
        this.service.login(NewUser, this);
    }
}