// TODO Faire une seule classe modale et des classe étendues
import EventService from "../services/EventService";
export default class Modale {
    overlay;
    modale;
    event;
    service;
    marker;
    rebourContainer;
    rebour;
    constructor(marker) {
        this.marker = marker;
        this.event = marker.event;

        // création des conteneurs modale et overlay
        this.overlay = document.createElement('div');
        this.modale = document.createElement('div');
        this.overlay.classList.add("overlay");
        this.modale.classList.add("modale");

        // affichage de l'overlay et de la modale
        document.body.append(this.overlay)
        document.body.append(this.modale)
        // écouteur sur l'0verlay
        this.overlay.addEventListener('click', this.removeModale.bind(this));
        // Ajout du contenu de la modale
        this.addModaleContent();
        this.compteRebour()

        this.service = new EventService()


    }

    addModaleContent() {
        // Ajout du bouton et de son container
        const containerButton = document.createElement('div');
        containerButton.classList.add("center");
        containerButton.classList.add("marg-top");
        const button = document.createElement('button');
        button.classList.add("supr");
        containerButton.append(button);
        button.innerHTML = `
        Supprimer l'évènement
        `
        // Contenu HTML de la modale
        this.modale.innerHTML = `
        <h2>${this.event.Title}</h2>
        <h3>Evènement dans :</h3>
        <p id="time"></p>
        <div class="separator"></div>
        <p>Du ${this.event.getDateStart()} au ${this.event.getDateEnd()}</p>
        <p class="marg-top">${this.event.Description}</p>
        `
        // Injection du bouton supprimé si un utilisateur est connecté
        if (User)
            this.modale.append(containerButton);
        // Attribution du conteneur du compte à rebour en propriété    
        this.rebourContainer = this.modale.querySelector('#time')
        // Ecouteur sur le bouton supprimé
        button.addEventListener('click', this.deleteEvent.bind(this))
        // attribution de la couleur de la modale en fonction de la couleur (statut) de l'évènement
        this.modale.style.backgroundColor = this.event.color;
    }
    removeModale() {
        this.overlay.remove();
        this.modale.remove();
        // Arrêt du compte à rebour
        clearInterval(this.rebour)
    }
    // Appel au service de suppréssion d'un évènement et suppréssion du marqueur
    deleteEvent() {
        this.service.deleteLocalEvent(this.event._id);
        this.marker.container.remove()
        this.removeModale();

    }
    // affiche du message sur le statut de l'évènement ou du compte à rebour
    compteRebour() {

        if (this.event.statut === 'ended') {
            this.rebourContainer.innerHTML = 'Quel dommage! Vous avez raté cet événement!'
            return
        }
        if (this.event.statut === 'progress') {
            this.rebourContainer.innerHTML = "L'évènement est commencé mais pas encore terminé!"
            return
        }
        // affichage du temps pour qu'il soit déjà présent avant les 1s de delai
        this.getTime();
        // enregistrement du compte à rebour toute les 1s
        this.rebour = setInterval(this.getTime.bind(this), 1000);
    }

    // injection de la différence de date dans le conteneur du compte à rebour
    getTime() {
        const time = this.event.displayDiffDate()
        this.rebourContainer.innerHTML = `${time[0]} jours, ${time[1]} heures, ${time[2]} minutes et ${time[3]} secondes`
    }
}