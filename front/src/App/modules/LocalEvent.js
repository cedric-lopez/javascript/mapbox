import Marker from "../Mapbox/Marker";

export default class LocalEvent {
    _id;
    Title;
    Description;
    DateStart;
    DateEnd;
    Lat;
    Lon;
    color;
    statut;
    marker;
    ObjectMap;
    constructor(json, ObjectMap) {
        // propriétés d'un évènement
        this._id = json._id;
        this.Title = json.Title;
        this.Description = json.Description;
        this.DateStart = json.DateStart;
        this.DateEnd = json.DateEnd;
        this.Lat = json.Lat;
        this.Lon = json.Lon;
        // Statut de l'évènement : terminé, en cours...
        this.statut = this.eventStatut();
        // Récupéra de l'objet map pour pouvoir ajouter les marqueur
        this.ObjectMap = ObjectMap;
        // Création d'un marqueur associé à l'évènement
        this.marker = new Marker(this)
        this.refresh();
    }
    // Converti une date en texte
    getDate(date) {
        return new Date(date).toLocaleDateString();
    }

    // Date de début de l'évènement en texte
    getDateStart() {
        return this.getDate(this.DateStart)
    }

    // Date de fin de l'évènement en texte
    getDateEnd() {
        return this.getDate(this.DateEnd)
    }

    // Renvoi la différence entre 2 dates en séparant les jours, heures, minutes et secondes
    displayDiffDate() {
        const diffMilliseconds = this.getDiffDateNow()
        let seconds = Math.floor(diffMilliseconds / 1000);
        let minutes = Math.floor(seconds / 60);
        let hours = Math.floor(minutes / 60);
        let days = Math.floor(hours / 24);
        seconds = seconds % 60
        minutes = minutes % 60
        hours = hours % 24
        return [days, hours, minutes, seconds]
    }
    // écart entre la date de début d'évènement et la date de maintenant en millisecondes
    getDiffDateNow() {
        return new Date(this.DateStart) - new Date();
    }

    // renvoie l'écart entre la date de début d'évènement et la date de maintenant nombre décimal de jours
    getDiffDateNowDays() {
        return this.getDiffDateNow() / (1000 * 60 * 60 * 24);
    }

    // Attribution du statut de l'évènement et de sa couleur associé
    eventStatut() {
        const days = this.getDiffDateNowDays()
        // évènement terminé
        if ((new Date(this.DateEnd) - new Date()) <= 0) {
            this.color = 'crimson'
            return 'ended'
        }
        // évènement en cours
        if ((new Date(this.DateStart) - new Date()) <= 0) {
            this.color = 'deeppink'
            return 'progress'
        }
        // évènement dans moins de 3 jours
        if (days < 3) {
            this.color = 'chocolate'
            return 'soon'
        }
        // évènement dans plus de 3 jours
        this.color = 'teal'
        return 'long'
    }

    // TODO corriger la modale qui revient en boucle

    // refresh() {

    //     setInterval(this.toto.bind(this), 1000);
    // }
    // toto() {
    //     const toto = this.eventStatut()
    //     console.log(toto);
    //     console.log(this.statut);
    //     if (toto === this.statut) {
    //         if (this.marker.modale) {
    //             this.marker.modale.removeModale();
    //             this.marker.container.remove();
    //             this.marker = new Marker(this);
    //             this.marker.displayModale()
    //             return
    //         }
    //         this.marker.container.remove();
    //         this.marker = new Marker(this);

    //     }
    // }
}