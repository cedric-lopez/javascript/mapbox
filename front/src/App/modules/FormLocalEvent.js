import EventService from "../services/EventService";
import LocalEvent from "./LocalEvent";


export default class FormLocalEvent {

    container;
    inputs = {};
    containerMessage;
    containersErrors = {};
    service = new EventService;
    ObjectMap;
    constructor(container, map) {
        // creation du conteneur du formulaire
        this.container = document.createElement('div');
        this.container.setAttribute("id", "form");

        // récupération de la map
        this.ObjectMap = map;

        this.render(container)
        this.addEventLocation();

    }
    // récupère la localisation au click sur la map
    addEventLocation() {
        this.ObjectMap.map.on('click',
            (e) =>
                this.addLocation(e));

    };

    // Ajout des coordonnées dans le formulaire
    addLocation(e) {

        this.inputs['Lat'].value = e.lngLat.lat;
        this.inputs['Lon'].value = e.lngLat.lng;

    }

    // Récupération des données du formulaire et appel au service d'ajout d'un évènement
    addEvent() {
        const NewEvent = {}
        for (let input in this.inputs) {
            NewEvent[`${input}`] = this.inputs[input].value;

        }
        NewEvent.DateStart = new Date(NewEvent.DateStart)
        NewEvent.DateEnd = new Date(NewEvent.DateEnd)
        this.service.postLocalEvent(NewEvent, this);

    }
    // Création d'un LocalEvent avec son marqueur
    createLocalEvent(data) {
        const event = new LocalEvent(data, this.ObjectMap)
    }
    // clear des chmaps du formulaire et appel à l'affiche des erreurs (vides) pour les vider
    clear() {
        for (let input in this.inputs) {
            this.inputs[input].value = null;
        }
        this.addErrors();
    }
    // reset des erreurs et affichage des messages d'erreurs dans les conteneurs d'erreurs
    addErrors(errors) {
        this.clearErrors();
        for (const error in errors) {
            this.containersErrors[`${error}`].innerHTML = errors[`${error}`].message;

        }
    }
    // resest des erreurs
    clearErrors() {

        for (const containerError in this.containersErrors) {
            this.containersErrors[`${containerError}`].innerHTML = "";
        }
    }

    // Ajout du code HTML du formulaire dans le conteneur du formulaire
    //attribution des containers dans leurs propriétés
    getContainer() {

        this.container.innerHTML = this.getContent();
        this.inputsContainers();
        this.submit();
        this.messageContainer();
        this.errorsContainers();

        return this.container
    }
    // attribution des champs de formulaire dans l'objet form
    inputsContainers() {

        const containersInputs = this.container.querySelectorAll('input,textarea');
        for (let value of containersInputs) {
            this.inputs[`${value.id}`] = value;
        }

    }
    // attribution du conteneur du message après validation du formulaire
    messageContainer() {
        this.containerMessage = this.container.querySelector('#message');
    }
    // attribution des conteneurs d'erreurs
    errorsContainers() {
        const arrayContainersErrors = this.container.getElementsByClassName('error');
        for (let value of arrayContainersErrors) {

            this.containersErrors[`${value.dataset.error}`] = value;
        }
    }
    // ajout de l'évènement de validation du formulaire
    submit() {
        const submitEvent = this.container.querySelector('#submit-event');
        submitEvent.addEventListener('click', this.addEvent.bind(this));
    }
    // affichage du formulaire dans son conteneur
    render(container) {

        container.append(this.getContainer())
    }

    // HTML du formulaire (Data-error et Id des inputs doivent correspondrent aux propriétés d'un LocalEvent)
    getContent() {
        return `

        <h1>Ajouter un évènement</h1>
        <div class='form-separator'></div>
        <div class='container'>
        <p id="message"></p>
            <div class='flex-column'>
            <p class="error"  Data-error="Title"></p>
                <label for="Title">Titre de l'évènement:</label>
        
                <input type="text" id="Title" name="title" required>
            </div>
            <div class='flex-column marg-top'>
            <p class="error"  Data-error="DateStart"></p>
            <label for="DateStart">Date de début:</label>
            <input type="datetime-local" id="DateStart" name="date-start">
           </div>
    
           <div class='flex-column marg-top'>
           <p class="error" Data-error="DateEnd"></p>
           <label for="DateEnd">Date de fin:</label>
           <input type="datetime-local" id="DateEnd" name="date-end">
          </div>
    
            <div class='flex-column marg-top'>
            <p class="error" Data-error="Description"></p>
                <label for="Description">Description:</label>  
                <textarea id="Description" name="description" rows="5" cols="33"></textarea>
            </div>
    
            <div class='flex-column marg-top'>
            <p>Entrez une position ou cliquez directement sur la map</p>
            <p class="error"  Data-error="Lat"></p>
            <label for="Lat">Latitude:</label>
            <input type="text" id="Lat" name="latitude" required>
            </div>
    
            <div class='flex-column marg-top'>
            <p class="error"  Data-error="Lon"></p>
            <label for="Lon">Longitude:</label>
            <input type="text" id="Lon" name="longitude" required>
            </div>
    
            <div class='center marg-top'>
            <button type="button" id="submit-event">
            Ajouter
            </button>
            </div>
        </div>
    
        </div>
    
        `
    }

}