
import AuthModale from "./AuthModale";

export default class Header {
    submit;
    container;
    page;
    constructor(page) {
        // Récupération de la page où a été instancié le header
        this.page = page;
        // création du container du header
        this.container = document.createElement('div');
        // Affichage dans le conteneur de la page
        this.render(this.page.headerContainer);
    }
    // HTMl du header
    content(buttonValue) {

        return `
        <button>${buttonValue}</button>
        `
    }
    getContainer() {
        this.addsubmit()
        return this.container
    }

    // Attribution de la valeur du bouton connexion/deconnexion
    // Attribution du bouton dans les propriétés du header
    addsubmit() {
        if (!User) {
            this.container.innerHTML = this.content('Connexion');
            this.submit = this.container.querySelector('button');
            this.submitForm();
        }
        else {
            this.container.innerHTML = this.content('Deconnexion');
            this.submit = this.container.querySelector('button');
            this.submitFormDeco();
        }

    }
    // ajout de l'évènement de validation du formulaire
    submitForm() {
        this.submit.addEventListener('click', this.displayModale.bind(this));
    }
    // ajout de l'évènement de validation du formulaire de déco
    submitFormDeco() {
        this.submit.addEventListener('click', this.deco.bind(this));
    }
    // création et affiche de la modale de connexion
    displayModale() {
        new AuthModale(this)
    }
    // Reset de la constante User,du token en localstorage et nouveau rendu de la page d'accueil (hors map)
    deco() {
        window.User = null;
        this.page.render();
        localStorage.removeItem('token');
    }
    // affichache du header dans son container
    render(container) {
        container.append(this.getContainer());
    }
}