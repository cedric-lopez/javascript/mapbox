
import 'mapbox-gl/dist/mapbox-gl.css';
import mapboxgl from 'mapbox-gl';
import Modale from '../modules/Modale'
export default class Marker {

    marker = null;
    color;
    container;
    info;
    event;
    modale;
    constructor(event) {
        this.event = event;
        this.color = this.event.color;
        this.marker = new mapboxgl.Marker(
            {
                color: this.color,
            }
        );
        this.marker.setLngLat({
            lon: this.event.Lon,
            lat: this.event.Lat
        });

        this.getContainer();
        this.displayInfo();
        this.info = document.createElement('div');
        this.info.classList.add("info");
        this.addMarker(this.event.ObjectMap.map)

    }

    addMarker(map) {
        this.marker.addTo(map);
    }
    getContainer() {
        this.container = this.marker._element;
    }
    addInfo() {
        this.info.innerHTML = `<h2>${this.event.Title}</h2>
        <div class="separator"></div>
        <p>${this.event.getDateStart()}</p>
        <p>${this.event.getDateEnd()}</p>`;
        this.container.append(this.info);
        this.info.style.backgroundColor = this.event.color;
    }
    removeInfo() {

        this.info.remove();
        this.info.innerHTML = '';
    }


    displayInfo() {
        this.container.addEventListener('mouseenter', this.addInfo.bind(this));
        this.container.addEventListener('mouseleave', this.removeInfo.bind(this));
        this.container.addEventListener('click', this.displayModale.bind(this));

    }


    displayModale() {
        this.modale = new Modale(this);

    }
}