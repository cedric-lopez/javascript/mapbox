import config from '../../../app.config';
import 'mapbox-gl/dist/mapbox-gl.css';
import mapboxgl from 'mapbox-gl';
import EventService from "../services/EventService";
import LocalEvent from '../modules/LocalEvent';
export default class Map {
    map = null;
    service;
    constructor() {
        // injection du token défini dans appConfig
        mapboxgl.accessToken = config.mapbox.token
        // coordonnées de limitation de la map
        const bounds = [
            [-150, -80],
            [150, 80]
        ];
        // création d'une map injectée dans la div main-map
        this.map = new mapboxgl.Map({
            container: 'main-map', // container ID
            style: 'mapbox://styles/mapbox/streets-v11', // style URL
            center: config.mapbox.locationDefault, // position de départ [lng, lat]
            zoom: 9, // zoom de départ
            maxBounds: bounds

        });
        this.service = new EventService();
        this.getLocalEvents()
    }

    new(data) {
        const marker = new Marker(data)
        marker.addMarker(this.map)
    }
    addEvents(localEvents) {
        for (let i in localEvents) {
            const event = new LocalEvent(localEvents[i], this)
        };
    };
    getLocalEvents() {
        this.service.getLocalEvents(this)
    };
}