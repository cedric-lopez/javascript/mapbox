const express = require('express');
const router = express.Router();
const localEventCtrl = require('../controllers/LocalEvent')
const auth = require('../midlleware/auth');


router.post('/', auth, localEventCtrl.createLocalEvent);
router.get('/', localEventCtrl.getAllLocalEvents);
router.delete('/:id', auth, localEventCtrl.deleteLocalEvent);



module.exports = router;
