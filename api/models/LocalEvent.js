const mongoose = require('mongoose');

const LocalEventSchema = mongoose.Schema({
  Title: {type: String,required: [true,'Veuillez renseigner un titre']},
  Description:{type: String,required: [true,'Veuillez renseigner une description'],},
  DateStart: {type: Date,required: [true,'Veuillez renseigner une date de début'],},
  DateEnd: {type: Date,required: [true,'Veuillez renseigner une date de fin'],},
  Lat:{ 
    type: Number ,required: [true,'Veuillez renseigner une latitude'],
    min:[-80,'latitude trop basse (minimum -80)'],
    max:[80,'latitude trop haute(maximum 80)']
  },
  Lon:{
     type: Number ,required: [true,'Veuillez renseigner une longitude'],
     min:[-150,'longitude trop basse (minimum -150)'],
     max:[150,'longitude trop haute (maximum 150)']
    },

});

module.exports = mongoose.model('LocalEvent', LocalEventSchema);
