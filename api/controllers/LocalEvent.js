const { json } = require('express');
const LocalEvent = require('../models/LocalEvent');

exports.createLocalEvent = (req, res) => {
  const localEvent = new LocalEvent({
    Title: req.body.Title,
    Description: req.body.Description,
    DateStart: req.body.DateStart,
    DateEnd: req.body.DateEnd,
    Lat: req.body.Lat,
    Lon: req.body.Lon,

  });

  localEvent.save().then(
    () => {
      res.status(201).json(localEvent);

    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};
exports.getAllLocalEvents = (req, res) => {
  LocalEvent.find().then(
    (localEvent) => {
      res.status(200).json(localEvent);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};
exports.deleteLocalEvent = (req, res) => {
  LocalEvent.deleteOne({ _id: req.params.id }).then(
    () => {
      res.status(200).json({
        message: "L'évènement a été supprimé"
      });
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

