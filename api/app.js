const express = require('express');
const mongoose = require('mongoose');
const LocalEventRoutes = require('./routes/LocalEvent');
const UserRoutes = require('./routes/User');
require('dotenv').config();

const app = express();
// TODO masquer le mdp de la bdd
const bddUser = process.env.BDD_USER;
const bddPass = process.env.BDD_PASS;
mongoose.connect(`mongodb+srv://${bddUser}:${bddPass}@riberal.nmldh.mongodb.net/test`,


    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => console.log('Connexion à MongoDB réussie !'))
    .catch(() => console.log('Connexion à MongoDB échouée !'));
app.use(express.json());
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});
app.use('/api/auth', UserRoutes)
app.use('/api/LocalEvent', LocalEventRoutes);
module.exports = app